import Agerange from "../components/Agerange";
import Howdoesitwork from "../components/Howdoesitwork";
import Getquote from "../components/Getquote";
import "../scss/pages/funerals/_.scss";
import Header from "../components/Header";
import Footer from "../components/Footer";
const Index = () => (
  <>
    <Header />
    <div className="container">
      <div className="icons">
        <img src="../static/Greyfb.png" />
        <img src="../static/greyTwitter.png" />
        <img src="../static/greyemail.png" />
      </div>
      <div className="breadcrumbs">
        <img src="../static/moneylogo.png" />
        <p>
          {" "}
          <b>Experts in Money </b>
          <br />
          Last updated :28 October 2019
        </p>
      </div>

      <div className="title">
        <h1>Brilliant Funeral Plans Taking the UK by Storm</h1>
        <p>1000s of Brits over 50 have acted fast to protect their loved ones from hefty bills.</p>
        <img src="../static/oldguybike.jpg" />
        <figcaption className="fig-caption">
          People over 50 are fighting back against 14 years of rising costs with this easy solution{" "}
        </figcaption>
      </div>
      <div className="content-1">
        <h3>
          {" "}
          1000s of Brits over 50 have acted fast to protect their loved ones from hefty bills.{" "}
        </h3>
        <p>
          {" "}
          <b>Funeral costs are rising at a shocking rate</b> – and people's loved ones could be left
          to shoulder the bill.{" "}
        </p>
        <p> Funeral plans used to be rare a decade ago. </p>
        <p>
          {" "}
          However, due to the ballooning costs, thousands of people over 50 have rushed to ‘lock in’
          current prices and
          <b>
            {" "}
            <u>safeguard themselves against the future.</u>
          </b>
        </p>
        <p>
          {" "}
          According to independent research, the average cost of a funeral is an enormous{" "}
          <b>
            £4,271<sup>1</sup>.
          </b>
        </p>
        <p>
          {" "}
          And that's before you take into account the price of flowers, catering, and estate
          administration costs, which the government refuses to pay.
        </p>
        <p>
          {" "}
          That's an eyewatering <b>4.7% </b>increase since 2017!<sup>1</sup>{" "}
        </p>
        <p>
          {" "}
          <b>
            {" "}
            A{" "}
            <b>
              <u>funeral plan</u>
            </b>{" "}
            can help protect you from this.{" "}
          </b>{" "}
        </p>
      </div>
      <Agerange />
      <img className="page-logo" src="../static/logo.svg" />
      <div className="graph">
        <p>
          Relieving you and your loved ones from financial and emotional stress, it will ensure the
          cost of your funeral <b>won't rocket upwards</b> over time.
        </p>
        <img classsName="graph " src="../static/cost-of-dying.jpg" />
        <p>
          With projections showing that total cost of dying look likely to continue rising, there
          has <b>never been a better time</b> to consider your financial future.
        </p>

        <p>
          If there’s no money saved up for your funeral, or your life insurance doesn’t cover it,
          <b> your relatives will have to pay these huge expenses.</b>
        </p>
      </div>
      <Howdoesitwork />
      <Agerange />
      <img className="page-logo" src="../static/logo.svg" />
      <h3> How Funeral Costs Can Affect Your Family's Life</h3>
      <p>
        Almost 12% of families experience <b>significant financial problems</b> when having to pay
        for their loved ones' funeral costs. They are forced to use savings, credits cards or even
        take out loans, sell belongings or borrow money from friends to pay the funeral costs.
      </p>
      <p>
        Also emotional costs of a funeral should not be underestimated with{" "}
        <b>17% of families feeling overwhelmed</b> when having to plan their loved ones' funeral
        <sup>1</sup>.
      </p>
      <Getquote />
      <img className="map" src="../static/uk-map.jpg" />
    </div>
    <Footer />
  </>
);
export default Index;
