import React from "react";
import Header from "../../components/50sheader";
import Footer from "../../components/50sFooter";

import "../../scss/pages/over-50s/_.scss";

const Page = () => {
  return (
    <>
      <Header />
      <div className="main-container">
        <span className="breadcrumbs">
          Insurance{" "}
          <img
            className="breadcrumbs-img"
            alt="breadcrumbs-arrow"
            src="../../static/over-50s/arrow.png"
          />
          <b className="bold-breadcrumbs">Over 50s Life Insurance</b>
        </span>{" "}
        <div className="title-container">
          <h1 className="title">
            UK Residents Are Buying Over 50 Polices In Record Numbers And Here's Why{" "}
          </h1>
          <h2 className="subtitle"> Don't miss out on this brilliant deal</h2>
          <div className="insurance-container">
            <p>
              By <b>Nathan Gilbert</b> | 15th October 2019
            </p>
            <span>INSURANCE</span>
          </div>
        </div>
        <div className="content-split">
          <div className="content-box">
            <img className="passport" src="../../static/over-50s/man_passport_1.jpg" />
            <div className="content-box-1">
              <h5>
                Learn the Surprising Reasons for the Remarkable Increase in the Number of People
                Taking Out a Over 50s Policy
              </h5>
              <p>
                Over 50 life insurance is one of the best ways to protect your family and their
                future when you pass away. Your loved ones can use this to help cover the cost of
                your funeral or enjoy as a cash gift. However even with all these great benefits,
                over 35% of UK adults don’t have any cover in place.
              </p>
              <h4 className="headers">Why don’t more seniors have cover?</h4>
              <p>
                With all of these great benefits, many people may think that the costs are high. The
                great news is that in today’s market, policies are actually very affordable when you
                know where to look.
              </p>
              <p>
                Most can’t believe that these rates are in fact real, but the truth is rates have
                dropped significantly for life insurance policies in recent years.
              </p>
            </div>
            <div className="testimonial-container">
              <div>
                <img src="../../static/over-50s/expert.jpg" />
                <span> Nathen Gilbert</span>
              </div>
              <p>
                Thanks to <u> CoverOver50.co.uk</u> it's now easy to get cover between; £1000 to
                20,000, starting from just £0.26<sup>*</sup> a day with no need for any medical
                questions, its easy for anyone over 50 to get a policy.- a real win!
              </p>
            </div>
            <div className="get-started-title">
              <h4 className="headers">Get started </h4>
              <p>Simply select your age</p>
            </div>
            <div className="get-started">
              <div>
                <div className="img-background">
                  <img src="../../static/over-50s/age_selector_1.png" />
                </div>
                <p> 40 </p>
              </div>
              <div>
                <div className="img-background">
                  <img src="../../static/over-50s/age_selector_2.png" />
                </div>
                <p> 40-49 </p>
              </div>
              <div>
                <div className="img-background">
                  <img src="../../static/over-50s/age_selector_3.png" />
                </div>
                <p> 50-59 </p>
              </div>
              <div>
                <div className="img-background">
                  <img src="../../static/over-50s/age_selector_4.png" />
                </div>
                <p> 60+ </p>
              </div>
            </div>

            <div className="content-box-2">
              <p>
                With all of these great benefits, many people may think that the costs are high. The
                great news is that in today’s market, policies are actually very affordable when you
                know where to look.
              </p>
              <p>
                Most can’t believe that these rates are in fact real, but the truth is rates have
                dropped significantly for life insurance policies in recent years.
              </p>
              <p>
                Because of this, smart consumers now use online tools like CoverOver40.co.uk™ to
                receive fast and free life insurance quotes – only available to people over 40
                (That’s right, people under 40 cannot get these prices).{" "}
              </p>
              <p>
                Their calculator works so well that you will be able to get prices and access to
                these rates in minutes. It’s no wonder why so many people are saving money since you
                are now able to use this simple tool rather than spending weeks trying to contact
                and compare options with various life insurance providers.
              </p>
            </div>
            <div className="green-box-title">
              <h6>Guaranteed Acceptance For Over 50s With No Medical</h6>
            </div>
            <div className="green-box">
              <div className="green-box-text">
                <p>
                  Finally, most think they will need to answer loads of health questions and
                  complete a medical. Cover Over 40 don't need either of these things. If you're a
                  UK resident aged 50 to 80 acceptance is guaranteed.
                </p>
                <p>If you're aged 50 to 80 you can't go wrong with Cover Over 40.</p>
                <ul className="green-box-list">
                  <li> You're guaranteed to be accepted</li>
                  <li> Choose your benefit amount</li>
                  <li> No Medical or Health Questions</li>
                  <li> Cover starts immediately</li>
                  <li> FREE Will Kit included with your policy</li>
                </ul>
              </div>
            </div>
            <h4 className="hears-how-header">Here's How You Do It</h4>
            <p className="span-text">
              <span>Step 1:</span> <b className="click-below">Click your gender below.</b>
            </p>
            <p className="span-text">
              <span>Step 2:</span> Take our quick 60 seconds survey, to survey, to receive your FREE
              quote.
            </p>
            <p className="click-below-text">
              The UK’s leading providers offer the coverage & benefits are clearly explained with no
              “fine print” and surprises. Get expert answers to any questions you have about life
              insurance for over 50's…
            </p>

            <div className="gender-container">
              <div className="gender-buttons">
                <div className="gender-button-captions">
                  <img className="male-img" src="../../static/over-50s/male.png" />
                  <figcaption className="male">Male</figcaption>
                </div>
                <div className="gender-button-captions">
                  <img className="female-img" src="../../static/over-50s/female.png" />
                  <figcaption className="female">Female</figcaption>
                </div>
              </div>
              <button className="learn-more"> LEARN MORE >>></button>
            </div>
          </div>

          <img className="slider" alt="over 50s banner" src="../../static/over-50s/slider.jpg" />
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Page;

// improve font size in Mobile
