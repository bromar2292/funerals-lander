import React from 'react'
import App, { Container } from 'next/app'
import Head from 'next/head'
import Layout from '../components/Layout'


class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <link
            key="favicon"
            rel="icon"
            type="image/png"
            href="/public/favicon.png"
          />
        </Head>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Container>
    )
  }
}

export default MyApp