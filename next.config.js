// next.config.js
const withSass = require('@zeit/next-sass')
const fs = require('fs')
const { join } = require('path')
const { promisify } = require('util')

const copyFile = promisify(fs.copyFile)

module.exports = withSass({
  exportPathMap: async (defaultPathMap, { dev, dir, outDir }) => {
    if (dev) {
      return defaultPathMap
    }
    await copyFile(
      join(dir, '/static/favicon.png'),
      join(outDir, 'favicon.png')
    )
    return defaultPathMap
  },
  webpack: config => {
    config.module.rules.push(
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'sass-loader',
            options: {
              sourceMap: false,
              resources: 'scss/*.scss'
            }
          }
        ]
      }
    )

    config.optimization.splitChunks = {
      cacheGroups: {
        styles: {
          name: n => {
            return n.context.substring(n.context.lastIndexOf('/') + 1)
          },
          test: c => {
            return (
              c.type.match('css/mini-extract') &&
              c._identifier.indexOf('/scss/pages/') !== -1
            )
          },
          chunks: 'all',
          minChunks: 1,
          priority: 1,
          enforce: true
        }
      }
    }

    return config
  }
})
