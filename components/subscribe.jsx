import React, { useState } from "react";

import axios from "axios";

const Subscribe = () => {
  const [values, setValues] = useState({ name: "", email: "" });
  const [errors, setErrors] = useState({});

  const [status, setStatus] = useState();
  const apiKey = "yvPaE6nMEk5wxyebYYr1ow";
  const formId = 1113155;
  const headers = {
    "Content-type": "application/json; charset=utf-8"
  };

  const URL = `https://api.convertkit.com/v3/forms/${formId}/subscribe`;

  const handleChange = event => {
    const { name, value } = event.target;

    setValues({
      ...values,
      [name]: value
    });
  };

  async function handleSubmit(event) {
    event.preventDefault();
    setErrors({});
    var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    let _errors = {};
    if (!values.name) {
      _errors.name = "Please enter your name";
    }
    if (!values.email) {
      _errors.email = "Please supply an email address ";
    } else if (!re.test(values.email)) {
      _errors.email = "Email address is invalid";
    }
    if (_errors.name || _errors.email) {
      _errors.button = "Please correct the input fields";
    }

    if (Object.keys(_errors).length) {
      setErrors({ ..._errors, button: "Please correct the input fields" });
      setStatus("fail");
      return;
    }
    // await validate(values);
    console.log("status", status);

    const body = {
      api_key: apiKey,
      first_name: values.name,
      email: values.email
    };
    console.log(body);
    try {
      const res = await axios.post(URL, body, { header: headers });
      console.log(res.data);
      setStatus("success");
      setErrors({ button: "You have successfully subscribed" });
    } catch (e) {
      console.log(e);

      setStatus("fail");
      setErrors({ button: "There was a problem processing your request" });
    }
  }

  return (
    <>
      {status}
      <div className="subscribe-container">
        <h5>SUBSCRIBE TO OUR NEWSLETTER</h5>
        <h7>We promise not to spam you</h7>
        <form className="form" onSubmit={handleSubmit}>
          <div>
            <label>
              Name <b>*</b>
            </label>

            <div>
              <input
                type="text"
                name="name"
                placeholder="Name"
                value={values.name}
                onChange={handleChange}
              ></input>

              {errors.name && <p className="error">{errors.name}</p>}
            </div>
          </div>
          <div>
            <label>
              Email <b>*</b>
            </label>

            <div>
              <input
                type="text"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
              ></input>
              {errors.email && <p className="error">{errors.email}</p>}
            </div>
          </div>

          <button type="submit ">Subscribe</button>
          {errors.button && (
            <p className={`${status === "fail" ? "error" : "success"}`}>{errors.button}</p>
          )}
        </form>
      </div>
    </>
  );
};

export default Subscribe;
