export default () => (
  <div className="how">
    <h3>How Can I Get a Quote?</h3>
    <p className="how-partners-title">
      Our partners currently have{" "}
      <strong>more than 300 free Funeral Plan consultations</strong> available
      to be claimed. Just follow the steps below to claim your free
      consultation.
    </p>
    <div className="how-list">
      <div className="how-list-flex">
        <span>1</span>
        <p>Step 1 - Click your location on the map below.</p>
      </div>
      <div className="how-list-flex">
        <span>2</span>
        <p>
          Step 2 - Answer a few simple questions to find the right plan for you.
        </p>
      </div>
    </div>
  </div>
);
