export default () => (
  <div className="how">
    <h3>How Does it Work?</h3>
    <div className="how-list">
      <div className="how-list-flex">
        <span>1</span>
        <p>
          <b>Just a few pounds a week for cover.</b> Funeral plans are
          surprisingly cheap, and for the worry they relieve it’s a tiny cost.
          You can get funeral planning from <b>as little as 60p a day.</b>
        </p>
      </div>
      <div className="how-list-flex">
        <span>2</span>
        <p>
          <b>No medical.</b> You almost certainly won’t need any health check.
          It only takes a few minutes – then you’re all set.
        </p>
      </div>
      <div className="how-list-flex">
        <span>3</span>
        <p>
          <b>Peace of mind.</b> Remove unnecessary stress for you and your
          family. By pre-paying for a funeral plan you can put all worry to one
          side.
        </p>
      </div>
      <div className="how-list-flex">
        <span>4</span>
        <p>
          <b>Freeze your basic funeral costs.</b> With funeral costs set to
          continue to rise, now is the time to take action.
        </p>
      </div>
      <div className="how-list-flex">
        <span>5</span>
        <p>
          <b>Over 50s Guaranteed</b> Acceptance is guaranteed for UK residents
          at least 50 years old.
        </p>
      </div>
    </div>
    <img src="../static/old-boat.jpg" />
  </div>
);
