export default () => (
  <div className="age-range-main-container">
    <div className="title-container">
      <h5 className="age-range-title">
        Eligible for A Funeral Plan? Tap your icon below to find out!
      </h5>
      <div className="age-range-subtitle">
        Unfortunately, you are not eligible if you are under 50
      </div>
    </div>
    <div className="age-range-container">
      {/* <div className="female-container"> */}
      <div className="female-container2">
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-01.svg"
            />
          </span>
          <p>
            50<sup>_</sup>59
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-02.svg"
            />
          </span>
          <p>
            60<sup>_</sup>69
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-03.svg"
            />
          </span>
          <p>
            70<sup>_</sup>79
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-04.svg"
            />
          </span>
          <p>
            80<sup>_</sup>85
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-05.svg"
            />
          </span>
          <p>85+</p>
        </div>
      </div>
      {/* </div> */}

      {/* <div className="male-container"> */}
      <div className="male-container2">
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-06.svg"
            />
          </span>
          <p>
            50<sup>_</sup>59
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-07.svg"
            />
          </span>
          <p>
            60<sup>_</sup>69
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-08.svg"
            />
          </span>
          <p>
            70<sup>_</sup>79
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-09.svg"
            />
          </span>
          <p>
            80<sup>_</sup>85
          </p>
        </div>
        <div className="age-range-box">
          <span className="img-container">
            <img
              className="female-age-1"
              src="http://insurance.expertsinmoney.com/sites/all/themes/cdn_v01/img/people-icons/icon-person-10.svg"
            />
          </span>
          <p>85+</p>
        </div>
        {/* </div> */}
      </div>
    </div>
  </div>
);
