export default () => (
  <div className="footer">
    <div className="footer-cta">
      <b>
        <u>Click here and find out if you qualify for a Funeral Plan!</u>
      </b>
    </div>
    <div className="footer-container">
      <div className="footer-address">
        <div className="footer-address-seperator">
          <h4> Registered Office :</h4>
          <p className="footer-addresses-text">
            MVF Global,ImpweiL Works,
            <br /> Block C, Perren Street,
            <br />
            NW5 London
            <br />
            United Kingdom
          </p>
        </div>

        <div className="footer-address-seperator2">
          <h4>Contact us:</h4>
          <p className="footer-addresses-text">
            Telephone:0207 428 74 09
            <br />
            Company Registration Number : 06951544
            <br />
            Company VAT Number: GB 207 7137 19
          </p>
        </div>
      </div>
      <div className="t-c">
        <p>
          Privacy Policy
          <br /> Terms and Conditions
          <br /> Cookie Policy
        </p>
      </div>
    </div>
  </div>
);
