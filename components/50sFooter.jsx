export default () => (
  <div className="footer-background">
    <div className="footer-container">
      <p className="rights">2019 All Rights Reserved</p>
      <p className="t-c">Privacy Policy | Terms & Conditions | Contact us</p>
    </div>
  </div>
);
