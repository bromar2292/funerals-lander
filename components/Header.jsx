export default () => (
  <div className="header-container">
    <span className="advert">Advertorial</span>

    <div className="header-title">
      <img src="../static/logo.svg" />
      <button>Get A Quick Quote</button>
    </div>
  </div>
);
