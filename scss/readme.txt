the grid widths and all the variables are specified in the /modules/config_vars.scss file - adjust them to your needs

if you want to use any of the specified colours - use function getColor('COLOR_NAME', 'OPTIONAL_COLOR_SHADE') - if not shade provided 'base' will be chosen

to use any of the margins or paddings specified in the config_vars use getMarginVal('md,lg,sm etc') or getPaddingVal('md,lg,sm etc')

breakpoints are defined as @include breakpoint($from: 'md or any other screen') { }

in /vendor/fonts - add your font paths and then specify them in the config variables

